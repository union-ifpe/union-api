package br.com.union.exception;

public class EmailAlreadyRegisteredException extends Exception {

    private static final long serialVersionUID = 1L;

    public EmailAlreadyRegisteredException() {

    }

    public EmailAlreadyRegisteredException(String message) {
        super(message);
    }

    public EmailAlreadyRegisteredException(String message, Exception e) {
        super(message, e);
    }
}
