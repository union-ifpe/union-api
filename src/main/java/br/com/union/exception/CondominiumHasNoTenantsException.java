package br.com.union.exception;

public class CondominiumHasNoTenantsException extends Exception {

    private static final long serialVersionUID = 1L;

    public CondominiumHasNoTenantsException() {

    }

    public CondominiumHasNoTenantsException(String message) {
        super(message);
    }

    public CondominiumHasNoTenantsException(String message, Exception e) {
        super(message, e);
    }
}
