package br.com.union.exception;

public class CondominiumNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public CondominiumNotFoundException() {

    }

    public CondominiumNotFoundException(String message) {
        super(message);
    }

    public CondominiumNotFoundException(String message, Exception e) {
        super(message, e);
    }
}
