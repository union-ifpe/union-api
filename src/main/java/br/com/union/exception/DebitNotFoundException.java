package br.com.union.exception;

public class DebitNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public DebitNotFoundException() {

    }

    public DebitNotFoundException(String message) {
        super(message);
    }

    public DebitNotFoundException(String message, Exception e) {
        super(message, e);
    }
}
