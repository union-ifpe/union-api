package br.com.union.exception;

public class UserIsNotCondominiumOwnerException extends Exception {

    private static final long serialVersionUID = 1L;

    public UserIsNotCondominiumOwnerException() {

    }

    public UserIsNotCondominiumOwnerException(String message) {
        super(message);
    }

    public UserIsNotCondominiumOwnerException(String message, Exception e) {
        super(message, e);
    }
}
