package br.com.union.exception;

public class UserNotEnabledException extends Exception{
    private static final long serialVersionUID = 1L;

    public UserNotEnabledException() {

    }

    public UserNotEnabledException(String message) {
        super(message);
    }

    public UserNotEnabledException(String message, Exception e) {
        super(message, e);
    }
}
