package br.com.union.exception;

public class EmptyFileException extends Exception {
    private static final long serialVersionUID = 1L;

    public EmptyFileException() {

    }

    public EmptyFileException(String message) {
        super(message);
    }

    public EmptyFileException(String message, Exception e) {
        super(message, e);
    }
}
