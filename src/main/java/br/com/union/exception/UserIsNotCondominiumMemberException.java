package br.com.union.exception;

public class UserIsNotCondominiumMemberException extends Exception {

    private static final long serialVersionUID = 1L;

    public UserIsNotCondominiumMemberException() {

    }

    public UserIsNotCondominiumMemberException(String message) {
        super(message);
    }

    public UserIsNotCondominiumMemberException(String message, Exception e) {
        super(message, e);
    }
}
