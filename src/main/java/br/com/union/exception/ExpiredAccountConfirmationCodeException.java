package br.com.union.exception;

public class ExpiredAccountConfirmationCodeException extends Exception {

    private static final long serialVersionUID = 1L;

    public ExpiredAccountConfirmationCodeException() {

    }

    public ExpiredAccountConfirmationCodeException(String message) {
        super(message);
    }

    public ExpiredAccountConfirmationCodeException(String message, Exception e) {
        super(message, e);
    }
}
