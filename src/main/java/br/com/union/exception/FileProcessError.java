package br.com.union.exception;

public class FileProcessError extends Exception {
    private static final long serialVersionUID = 1L;

    public FileProcessError() {

    }

    public FileProcessError(String message) {
        super(message);
    }

    public FileProcessError(String message, Exception e) {
        super(message, e);
    }
}
