package br.com.union.repository;

import br.com.union.model.UserSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserSessionDAO extends JpaRepository<UserSession, Long> {

    @Query("SELECT session FROM UserSession session WHERE deleted = false")
    public List<UserSession> listActiveSessions();

    @Query("SELECT session FROM UserSession session WHERE session.token = :token AND session.deleted = false")
    public UserSession findByToken(@Param("token") String token);

    @Query("SELECT session FROM UserSession session WHERE session.user.unionIdentifier = :unionIdentifier AND session.deleted = false")
    public UserSession findByUserUUID(@Param("unionIdentifier") String userUUID);

    @Modifying
    @Query("UPDATE UserSession SET deleted = true, logoutDate = now() WHERE token = :token")
    public void delete(@Param("token") String token);
}
