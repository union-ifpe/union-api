package br.com.union.repository;

import br.com.union.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantDAO extends JpaRepository<Tenant, Long> {

    @Query("SELECT t FROM Tenant t WHERE t.user IS NOT NULL AND t.user.unionIdentifier = :userUUID")
    public Tenant fetchTenantByUser(@Param("userUUID") String userUUID);
}
