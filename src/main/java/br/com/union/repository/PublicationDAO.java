package br.com.union.repository;

import br.com.union.model.Condominium;
import br.com.union.model.Publication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublicationDAO extends JpaRepository<Publication, Long> {

    public Publication findByUnionIdentifier(String unionIdentifier);

    public List<Publication> findByCondominium(Condominium condominium);

    @Query("SELECT COUNT(p.id) FROM Publication p JOIN p.condominium c WHERE c.unionIdentifier = :condominiumUUID")
    public Integer getPublicationsCountByCondominium(@Param("condominiumUUID") String condominiumUUID);
}
