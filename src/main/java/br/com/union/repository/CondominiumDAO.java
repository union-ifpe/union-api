package br.com.union.repository;

import br.com.union.model.Condominium;
import br.com.union.model.Tenant;
import br.com.union.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CondominiumDAO extends JpaRepository<Condominium, Long> {

    public Condominium findByUnionIdentifierAndDeletedFalse(String unionIdentifier);

    @Query("SELECT c FROM Condominium c JOIN c.tenants t WHERE c.owner.email != :email and t.email = :email and t.user is null and c.name LIKE CONCAT('%', :name, '%')")
    public List<Condominium> listAvailablesByName(@Param("email") String email, @Param("name") String name);

    @Query("SELECT DISTINCT c FROM Condominium c LEFT JOIN c.tenants t WHERE (c.owner.email = :email OR t.email = :email AND t.user IS NOT NULL) AND c.deleted = false AND c.name LIKE CONCAT('%', :name, '%')")
    public List<Condominium> listByUser(@Param("email") String email, @Param("name") String name);

    @Query("SELECT COUNT(t.id) FROM Condominium c JOIN c.tenants t WHERE t.user IS NOT NULL AND c.unionIdentifier = :condominiumUUID")
    public Integer getTenantsCountByCondominium(@Param("condominiumUUID") String condominiumUUID);

    @Query("SELECT t.user FROM Condominium c JOIN c.tenants t WHERE t.user IS NOT NULL AND c.unionIdentifier = :condominiumUUID")
    public List<User> listTenantsByCondominium(@Param("condominiumUUID") String condominiumUUID);

    @Query("SELECT c.tenants FROM Condominium c JOIN c.tenants t WHERE t.user IS NOT NULL AND t.user.unionIdentifier = :userUUID AND c.unionIdentifier = :condominiumUUID")
    public Tenant fetchTenantByCondominiumAndUser(@Param("condominiumUUID") String condominiumUUID,
                                                  @Param("userUUID") String userUUID);
}
