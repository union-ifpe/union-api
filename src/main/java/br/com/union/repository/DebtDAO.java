package br.com.union.repository;

import br.com.union.model.Condominium;
import br.com.union.model.Debt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DebtDAO extends JpaRepository<Debt, Long> {

    @Query("SELECT d FROM Debt d JOIN d.userDebts ud WHERE ud.paid = false AND ud.tenant.user IS NOT NULL AND ud.tenant.user.unionIdentifier = :userUUID AND d.condominium.unionIdentifier = :condominiumUUID")
    public List<Debt> listOpenDebtsByTenant(@Param("userUUID") String userUUID,
                                            @Param("condominiumUUID") String condominiumUUID);

    @Query("SELECT d FROM Debt d JOIN d.userDebts ud WHERE ud.paid = true AND ud.tenant.user IS NOT NULL AND ud.tenant.user.unionIdentifier = :userUUID AND d.condominium.unionIdentifier = :condominiumUUID")
    public List<Debt> listDebtsHistory(@Param("userUUID") String userUUID,
                                       @Param("condominiumUUID") String condominiumUUID);

    @Query("SELECT d FROM Debt d JOIN d.condominium c WHERE c.unionIdentifier = :condominiumUUID AND c.owner.unionIdentifier = :userUUID")
    public List<Debt> listDebtsByCondominiumOwner(@Param("userUUID") String userUUID,
                                                  @Param("condominiumUUID") String condominiumUUID);

    @Query("SELECT d FROM Debt d JOIN d.userDebts ud WHERE ud.tenant.email = :email AND d.condominium.unionIdentifier = :condominiumUUID")
    public List<Debt> listDebtsByTenant(@Param("email") String email, @Param("condominiumUUID") String condominiumUUID);

    public Debt findByUnionIdentifier(String unionIdentifier);

    public List<Debt> findByCondominium(Condominium condominium);
}
