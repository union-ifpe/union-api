package br.com.union.repository;

import br.com.union.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDAO extends JpaRepository<User, Long> {

    public User findByEmail(String email);

    public User findByEmailAndEnabledTrue(String email);

    public User findByUnionIdentifierAndEnabledTrue(String unionIdentifier);

    public User findByUnionIdentifier(String unionIdentifier);

    public List<User> findByEnabledFalse();
}
