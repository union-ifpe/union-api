package br.com.union.repository;

import br.com.union.model.IdentityConfirmation;
import br.com.union.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IdentityConfirmationDAO extends JpaRepository<IdentityConfirmation, Long> {

    public IdentityConfirmation findByCode(String code);

    public List<IdentityConfirmation> findByUser(User user);
}
