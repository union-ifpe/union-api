package br.com.union.repository;

import br.com.union.model.User;
import br.com.union.model.UserDebt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDebtDAO extends JpaRepository<UserDebt, Long> {

    @Query("SELECT ud.tenant.user FROM UserDebt ud WHERE ud.paid = false AND ud.debt.unionIdentifier = :debtUUID AND ud.debt.condominium.owner.unionIdentifier = :userUUID")
    public List<User> listUsersByDebt(@Param("debtUUID") String debtUUID, @Param("userUUID") String userUUID);

    @Query("SELECT ud FROM UserDebt ud WHERE ud.tenant.user IS NOT NULL AND ud.tenant.user.unionIdentifier = :userUUID AND ud.debt.unionIdentifier = :debtUUID")
    public UserDebt fetchDebtByUser(@Param("debtUUID") String debtUUID, @Param("userUUID") String userUUID);
}
