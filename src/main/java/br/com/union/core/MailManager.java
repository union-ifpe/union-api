package br.com.union.core;

import br.com.union.config.WebClientRest;
import br.com.union.model.transport.MailDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class MailManager {

    private static final String DEFAULT_URI = "http://ec2-35-170-243-120.compute-1.amazonaws.com:8082";

    @Value("${mail:unionapp.suporte@gmail.com}")
    private String from;

    private WebClientRest webClientRest;

    public MailManager() {
        this.webClientRest = new WebClientRest();
    }

    public void sendMail(MailDTO mailDTO) throws Exception {

        if (mailDTO.getFrom() == null) {
            mailDTO.setFrom(this.from);
        }

        String uri = new StringBuilder().append(DEFAULT_URI).append("/mail/send").toString();
        ResponseEntity<Void> response = this.webClientRest.post(uri, Void.class, mailDTO);

        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Error on send email");
        }

    }
}
