package br.com.union.core;

import br.com.union.config.WebClientRest;
import br.com.union.model.ImagesS3;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

@Component
public class ImagesManager {

    private static final String DEFAULT_URI = "http://ec2-107-23-45-162.compute-1.amazonaws.com:9090";

    private WebClientRest webClientRest;

    public ImagesManager() {
        this.webClientRest = new WebClientRest();
    }

    public ResponseEntity<ImagesS3> sendImage(String unionIdentifier, MultipartFile file) throws IOException {
        String uri = new StringJoiner("/").add(DEFAULT_URI).add("images").toString();

        Map<String, String> headers = new HashMap<>();
        headers.put("bucketCategory", "union-profile");

        Map<String, Object> params = new HashMap<>();
        params.put("file", file);
        params.put("imageName", unionIdentifier);

        ResponseEntity<ImagesS3> response = this.webClientRest.post(uri, ImagesS3.class, file, unionIdentifier, headers);

        return response;
    }
}
