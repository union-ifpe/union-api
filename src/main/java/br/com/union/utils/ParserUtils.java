package br.com.union.utils;

import br.com.union.exception.EmptyFileException;
import br.com.union.exception.FileProcessError;
import br.com.union.model.parsers.TenantParserDTO;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

public class ParserUtils {

    public static ParserUtils getInstance() {
        return new ParserUtils();
    }

    public List<TenantParserDTO> parse(MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new EmptyFileException("CSV cannot be parsed because is empty");
        }

        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

            CsvToBean<TenantParserDTO> csvToBean = new CsvToBeanBuilder<TenantParserDTO>(reader)
                    .withType(TenantParserDTO.class).build();

            List<TenantParserDTO> tenants = csvToBean.parse();
            return tenants;
        } catch (Exception e) {
            throw new FileProcessError(e.getMessage(), e);
        }
    }
}
