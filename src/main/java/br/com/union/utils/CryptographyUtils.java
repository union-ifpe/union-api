package br.com.union.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class CryptographyUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CryptographyUtils.class);

    public static CryptographyUtils getInstance() {
        return new CryptographyUtils();
    }

    /**
     * Generates an array with 32 random bytes and then returns it converted to
     * String
     *
     * @return A random String for password salt
     * @author Ronyeri Marinho
     */
    public String generatePasswordSalt() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] passwordSalt = new byte[32];
        secureRandom.nextBytes(passwordSalt);

        return DatatypeConverter.printHexBinary(passwordSalt).toUpperCase();
    }

    /**
     * Generates a SHA-512 hash of the salt concatenated password for increased
     * security
     *
     * @return the created password hash
     * @author Ronyeri Marinho
     */
    public String createPasswordWithSalt(String password, String salt) throws NoSuchAlgorithmException {
        try {
            String passwordWithSalt = new StringBuilder().append(salt).append(password).toString();

            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(passwordWithSalt.getBytes(StandardCharsets.UTF_8));

            return DatatypeConverter.printHexBinary(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage());
            throw new NoSuchAlgorithmException("Error on SHA-512 algorithm");
        }
    }

}
