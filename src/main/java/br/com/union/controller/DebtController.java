package br.com.union.controller;

import br.com.union.model.transport.DebtDTO;
import br.com.union.model.transport.UserDTO;
import br.com.union.service.DebtService;
import br.com.union.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/debt")
public class DebtController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DebtController.class);

    private UserService userService;

    private DebtService debtService;

    public DebtController(UserService userService, DebtService debtService) {
        this.userService = userService;
        this.debtService = debtService;
    }

    @GetMapping("/{debtUUID}")
    public ResponseEntity<DebtDTO> getDebt(@RequestHeader("token") final String token,
                                           @PathVariable("debtUUID") String debtUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            DebtDTO response = this.debtService.findByUnionIdentifier(debtUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/{debtUUID}/user")
    public ResponseEntity<List<UserDTO>> listUsersByDebt(@RequestHeader("token") final String token,
                                                         @PathVariable("debtUUID") String debtUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            List<UserDTO> response = this.debtService.listUsersByDebt(debtUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PutMapping("/{debtUUID}/user/{userUUID}/payment")
    public ResponseEntity<Void> confirmPayment(@RequestHeader("token") final String token,
                                               @PathVariable("debtUUID") String debtUUID, @PathVariable("userUUID") String userUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            this.debtService.confirmDebtPayment(userUUID, debtUUID, userInSession);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }
}
