package br.com.union.controller.schedulers;

import br.com.union.service.UserSessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableAsync
public class CheckUserSessionScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckUserSessionScheduler.class);

    private UserSessionService userSessionService;

    public CheckUserSessionScheduler(UserSessionService userSessionService) {
        this.userSessionService = userSessionService;
    }

    @Async
    @Scheduled(fixedDelay = 180000)
    public void clearUserSessionExpired() {
        LOGGER.info("Fetching and terminating expired sessions...");
        this.userSessionService.clearExpiredSessions();
    }

}
