package br.com.union.controller.schedulers;

import br.com.union.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableAsync
public class AlertInactiveAccountsScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlertInactiveAccountsScheduler.class);

    private UserService userService;

    public AlertInactiveAccountsScheduler(UserService userService) {
        this.userService = userService;
    }

    @Async
    @Scheduled(fixedDelay = 180000)
    public void deleteInactiveUsers() {
        LOGGER.info("Fetching and alert inactive users...");
        this.userService.alertDisabledAccounts();
    }

}
