package br.com.union.controller;

import br.com.union.exception.UserNotFoundException;
import br.com.union.model.transport.LoginDTO;
import br.com.union.model.transport.NewPasswordDTO;
import br.com.union.model.transport.UserDTO;
import br.com.union.service.IdentityConfirmationService;
import br.com.union.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    private IdentityConfirmationService accountConfirmationService;

    public UserController(UserService userService, IdentityConfirmationService accountConfirmationService) {
        this.userService = userService;
        this.accountConfirmationService = accountConfirmationService;
    }

    @GetMapping
    public ResponseEntity<UserDTO> get(@RequestHeader("token") final String token) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            return ResponseEntity.ok(userInSession);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@Valid @RequestBody UserDTO userDTO) throws Exception {
        try {
            UserDTO response = this.userService.createUser(userDTO);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/account-confirmation")
    public ResponseEntity<UserDTO> confirmAccount(@RequestBody String code) throws Exception {
        try {
            this.accountConfirmationService.confirmAccount(code);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/request-new-password")
    public ResponseEntity<UserDTO> sendRecoveryPasswordEmail(@RequestBody String email) throws Exception {
        try {
            this.userService.confirmEmailToRecoveryPassword(email);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PutMapping("/new-password")
    public ResponseEntity<UserDTO> changePassword(@RequestBody NewPasswordDTO newPassDTO) throws Exception {
        try {
            this.userService.changePassword(newPassDTO);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@Valid @RequestBody LoginDTO loginDTO) throws Exception {
        try {
            String response = this.userService.login(loginDTO);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout(@RequestHeader("token") String token) throws Exception {
        try {
            this.userService.logout(token);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/photo-profile")
    public ResponseEntity<UserDTO> uploadPhotoProfile(@RequestParam("file") MultipartFile file,
                                                      @RequestParam("unionIdentifier") String unionIdentifier) {
        try {
            return ResponseEntity.ok(userService.uploadPhoto(unionIdentifier, file));
        } catch (UserNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
