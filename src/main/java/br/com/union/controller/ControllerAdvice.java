package br.com.union.controller;

import br.com.union.exception.InvalidPasswordException;
import br.com.union.exception.UserNotEnabledException;
import br.com.union.model.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@RestControllerAdvice
public class ControllerAdvice {

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UserNotEnabledException.class)
    public Object handleUserNotEnabledException(UserNotEnabledException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),HttpStatus.BAD_REQUEST.value());

        return errorMessage;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidPasswordException.class)
    public Object handleInvalidPasswordException(InvalidPasswordException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),HttpStatus.BAD_REQUEST.value());

        return errorMessage;
    }


    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public Object handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),HttpStatus.BAD_REQUEST.value());

        return errorMessage;
    }


}
