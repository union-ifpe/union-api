package br.com.union.controller;

import br.com.union.exception.CondominiumNotFoundException;
import br.com.union.model.transport.*;
import br.com.union.service.CondominiumService;
import br.com.union.service.DebtService;
import br.com.union.service.PublicationService;
import br.com.union.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/condominium")
public class CondominiumController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CondominiumController.class);

    private CondominiumService condominiumService;

    private UserService userService;

    private PublicationService publicationService;

    private DebtService debtService;

    public CondominiumController(CondominiumService condominiumService, UserService userService,
                                 PublicationService publicationService, DebtService debtService) {
        this.condominiumService = condominiumService;
        this.userService = userService;
        this.publicationService = publicationService;
        this.debtService = debtService;
    }

    @GetMapping
    public ResponseEntity<Set<CondominiumDTO>> listByUser(@RequestParam("name") String name,
                                                          @RequestHeader("token") final String token) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            Set<CondominiumDTO> response = this.condominiumService.listByUser(userInSession, name);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/{condominiumUUID}")
    public ResponseEntity<CondominiumDTO> get(@RequestHeader("token") final String token,
                                              @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            CondominiumDTO response = this.condominiumService.findByUnionIdentifierAndUser(condominiumUUID,
                    userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping
    public ResponseEntity<CondominiumDTO> create(@RequestHeader("token") final String token,
                                                 @Valid @RequestBody CondominiumDTO condominiumDTO) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            CondominiumDTO response = this.condominiumService.create(condominiumDTO, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @DeleteMapping("/{condominiumUUID}")
    public ResponseEntity<Void> delete(@RequestHeader("token") final String token,
                                       @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            this.condominiumService.delete(condominiumUUID, userInSession);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/availables")
    public ResponseEntity<Set<CondominiumDTO>> searchAvailablesByName(@RequestParam("name") String name,
                                                                      @RequestHeader("token") final String token) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            Set<CondominiumDTO> response = this.condominiumService.searchAvailablesByName(userInSession, name);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/{condominiumUUID}/tenant")
    public ResponseEntity<List<TenantDTO>> insertTenants(@RequestHeader("token") final String token,
                                                         @PathVariable("condominiumUUID") String condominiumUUID, @RequestParam("tenants") MultipartFile tenantsCsv)
            throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            List<TenantDTO> response = this.condominiumService.insertTenants(tenantsCsv, condominiumUUID,
                    userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/{condominiumUUID}/tenant")
    public ResponseEntity<List<UserDTO>> listTenants(@RequestHeader("token") final String token,
                                                     @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            List<UserDTO> response = this.condominiumService.listTenants(condominiumUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @DeleteMapping("/{condominiumUUID}/tenant/leave")
    public ResponseEntity<Void> leaveCondominium(@RequestHeader("token") final String token,
                                                 @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            this.condominiumService.leaveCondominium(condominiumUUID, userInSession);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @DeleteMapping("/{condominiumUUID}/tenant/{userUUID}")
    public ResponseEntity<Void> removeTenant(@RequestHeader("token") final String token,
                                             @PathVariable("condominiumUUID") String condominiumUUID, @PathVariable("userUUID") String userUUID)
            throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            this.condominiumService.removeTenant(condominiumUUID, userUUID, userInSession);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/{condominiumUUID}/debt")
    public ResponseEntity<DebtDTO> createDebt(@RequestHeader("token") final String token,
                                              @PathVariable("condominiumUUID") String condominiumUUID, @RequestBody @Valid DebtDTO debtDTO)
            throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            DebtDTO response = this.debtService.create(debtDTO, condominiumUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/{condominiumUUID}/debt")
    public ResponseEntity<List<DebtDTO>> listByOwner(@RequestHeader("token") final String token,
                                                     @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            List<DebtDTO> response = this.debtService.listByCondominiumOwner(condominiumUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/{condominiumUUID}/debt/open")
    public ResponseEntity<List<DebtDTO>> listOpenDebtsByTenant(@RequestHeader("token") final String token,
                                                               @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            List<DebtDTO> response = this.debtService.listOpenDebtsByTenant(condominiumUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/{condominiumUUID}/debt/history")
    public ResponseEntity<List<DebtDTO>> listDebtsHistoryByTenant(@RequestHeader("token") final String token,
                                                                  @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            List<DebtDTO> response = this.debtService.listDebtsHistory(condominiumUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PutMapping("/{condominiumUUID}/tenant")
    public ResponseEntity<Void> enterCondominium(@RequestHeader("token") final String token,
                                                 @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            this.condominiumService.enterCondominium(condominiumUUID, userInSession);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/{condominiumUUID}/publication")
    public ResponseEntity<List<PublicationDTO>> listPublications(@RequestHeader("token") final String token,
                                                                 @PathVariable("condominiumUUID") String condominiumUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            List<PublicationDTO> response = this.publicationService.listByUser(condominiumUUID, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/{condominiumUUID}/publication")
    public ResponseEntity<PublicationDTO> publishMessage(@RequestHeader("token") final String token,
                                                         @PathVariable("condominiumUUID") String condominiumUUID, @RequestBody PublicationDTO publicationDTO)
            throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            PublicationDTO response = this.publicationService.create(condominiumUUID, publicationDTO, userInSession);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @DeleteMapping("/{condominiumUUID}/publication/{publicationUUID}")
    public ResponseEntity<Void> deletePublication(@RequestHeader("token") final String token,
                                                  @PathVariable("condominiumUUID") String condominiumUUID,
                                                  @PathVariable("publicationUUID") String publicationUUID) throws Exception {
        try {
            UserDTO userInSession = this.userService.getUserByToken(token);
            this.publicationService.delete(condominiumUUID, publicationUUID, userInSession);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PostMapping("/photo-profile")
    public ResponseEntity<CondominiumDTO> uploadPhotoProfile(@RequestParam("file") MultipartFile file,
                                                             @RequestParam("unionIdentifier") String unionIdentifier) {
        try {
            return ResponseEntity.ok(condominiumService.uploadPhoto(unionIdentifier, file));
        } catch (CondominiumNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
