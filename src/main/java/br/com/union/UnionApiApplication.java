package br.com.union;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UnionApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnionApiApplication.class, args);
    }

}
