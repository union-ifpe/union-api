package br.com.union.service;

import br.com.union.core.MailManager;
import br.com.union.model.IdentityConfirmation;
import br.com.union.model.User;
import br.com.union.model.transport.MailDTO;
import br.com.union.utils.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

    private static final String FROM = "unionapp.suporte@gmail.com";

    private MailManager mailManager;
    private IdentityConfirmationService identityConfirmationService;

    public MailService(IdentityConfirmationService identityConfirmationService) {
        this.mailManager = new MailManager();
        this.identityConfirmationService = identityConfirmationService;
    }

    @Deprecated
    public void sendEmail(@Valid MailDTO mailDTO) throws Exception {
        try {
            if (mailDTO.getFrom() == null) {
                mailDTO.setFrom(FROM);
            }
            this.mailManager.sendMail(mailDTO);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    @Deprecated
    public void sendCreateCondominiumMessage(User user, String condominium) throws Exception {
        String subject = "Novo condomínio";
        String template = "new-condominium-created.html";

        Map<String, Object> properties = new HashMap<>();
        properties.put("username", user.getName());
        properties.put("condominiumName", condominium);
        properties.put("currentYear", LocalDateTime.now().getYear());
        MailDTO mailDTO = new MailDTO(user.getEmail(), subject, template, properties);
        mailDTO.setHasAttachment(true);

        this.sendEmail(mailDTO);
    }

    @Deprecated
    public void sendConfirmationAccountEmail(User user) throws Exception {
        String subject = "Confirmação de cadastro";
        String template = "confirmation-account.html";

        String code = RandomUtils.getInstance().generateRandomCode();
        IdentityConfirmation accountConfirmation = new IdentityConfirmation();
        accountConfirmation.setCode(code);
        accountConfirmation.setUser(user);
        this.identityConfirmationService.save(accountConfirmation);

        Map<String, Object> properties = new HashMap<>();
        properties.put("username", user.getName());
        properties.put("confirmationCode", code);
        properties.put("currentYear", LocalDateTime.now().getYear());
        MailDTO mailDTO = new MailDTO(user.getEmail(), subject, template, properties);

        this.sendEmail(mailDTO);
    }

    @Deprecated
    public void sendRecoveryPasswordCode(User user) throws Exception {
        String subject = "Recuperação de senha";
        String template = "password-recovery.html";

        String code = RandomUtils.getInstance().generateRandomCode();
        IdentityConfirmation accountConfirmation = new IdentityConfirmation();
        accountConfirmation.setCode(code);
        accountConfirmation.setUser(user);
        this.identityConfirmationService.save(accountConfirmation);

        Map<String, Object> properties = new HashMap<>();
        properties.put("username", user.getName());
        properties.put("confirmationCode", code);
        properties.put("currentYear", LocalDateTime.now().getYear());
        MailDTO mailDTO = new MailDTO(user.getEmail(), subject, template, properties);

        this.sendEmail(mailDTO);
    }

    @Deprecated
    public void sendAccountReminderEmail(User user) throws Exception {
        String subject = "Lembrete: Confirmação de cadastro";
        String template = "confirmation-account-alert.html";

        Map<String, Object> properties = new HashMap<>();
        properties.put("username", user.getName());
        properties.put("currentYear", LocalDateTime.now().getYear());
        MailDTO mailDTO = new MailDTO(user.getEmail(), subject, template, properties);

        this.sendEmail(mailDTO);
    }

}
