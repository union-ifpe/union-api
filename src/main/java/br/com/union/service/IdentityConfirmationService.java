package br.com.union.service;

import br.com.union.exception.ExpiredAccountConfirmationCodeException;
import br.com.union.model.IdentityConfirmation;
import br.com.union.model.User;
import br.com.union.repository.IdentityConfirmationDAO;
import br.com.union.repository.UserDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class IdentityConfirmationService {

    private IdentityConfirmationDAO identityConfirmationDAO;

    private UserDAO userDAO;

    public IdentityConfirmationService(IdentityConfirmationDAO identityConfirmationDAO, UserDAO userDAO) {
        super();
        this.identityConfirmationDAO = identityConfirmationDAO;
        this.userDAO = userDAO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save(IdentityConfirmation accountConfirmation) {
        this.identityConfirmationDAO.save(accountConfirmation);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void confirmAccount(String code) throws Exception {
        IdentityConfirmation identityConfirmation = this.verifyCode(code);
        User user = identityConfirmation.getUser();
        user.setEnabled(true);
        this.userDAO.save(user);
        this.identityConfirmationDAO.delete(identityConfirmation);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void confirmCodeToRecoveryPassword(String code, User user) throws Exception {
        IdentityConfirmation identityConfirmation = this.verifyCode(code);

        if (!user.getUnionIdentifier().equals(identityConfirmation.getUser().getUnionIdentifier())) {
            throw new IllegalArgumentException("The provided code is not valid");
        }

        this.identityConfirmationDAO.delete(identityConfirmation);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteByUser(User user) throws Exception {
        List<IdentityConfirmation> identitiesByUser = this.identityConfirmationDAO.findByUser(user);

        if (identitiesByUser != null) {
            identitiesByUser.forEach(identityConfirmation -> {
                this.identityConfirmationDAO.delete(identityConfirmation);
            });
        }
    }

    private IdentityConfirmation verifyCode(String code) throws Exception {
        IdentityConfirmation identityConfirmation = this.identityConfirmationDAO.findByCode(code);

        if (identityConfirmation == null) {
            throw new Exception("The code provided is invalid or expired");
        }

        if (LocalDateTime.now().isAfter(identityConfirmation.getExpirationDate())) {
            this.identityConfirmationDAO.delete(identityConfirmation);
            throw new ExpiredAccountConfirmationCodeException("The code provided is invalid or expired");
        }

        return identityConfirmation;
    }
}
