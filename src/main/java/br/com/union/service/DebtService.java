package br.com.union.service;

import br.com.union.exception.DebitNotFoundException;
import br.com.union.exception.UserIsNotCondominiumOwnerException;
import br.com.union.model.Condominium;
import br.com.union.model.Debt;
import br.com.union.model.User;
import br.com.union.model.UserDebt;
import br.com.union.model.transport.DebtDTO;
import br.com.union.model.transport.UserDTO;
import br.com.union.repository.DebtDAO;
import br.com.union.repository.UserDebtDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DebtService {

    private DebtDAO debtDAO;

    private UserDebtDAO userDebtDAO;

    private CondominiumService condominiumService;

    public DebtService(DebtDAO debtDAO, UserDebtDAO userDebtDAO, CondominiumService condominiumService) {
        this.debtDAO = debtDAO;
        this.userDebtDAO = userDebtDAO;
        this.condominiumService = condominiumService;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public DebtDTO findByUnionIdentifier(String debtUUID, UserDTO userInSession) throws Exception {
        Debt debt = this.debtDAO.findByUnionIdentifier(debtUUID);
        if (debt == null) {
            throw new Exception("Debt not found: " + debtUUID);
        }

        return new DebtDTO(debt);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<DebtDTO> listByCondominiumOwner(String condominiumUUID, UserDTO userInSession) throws Exception {
        List<Debt> debtsByOwner = this.debtDAO.listDebtsByCondominiumOwner(userInSession.getUnionIdentifier(),
                condominiumUUID);

        if (debtsByOwner == null) {
            return new ArrayList<>();
        }

        List<DebtDTO> debtsDTO = debtsByOwner.stream().map(DebtDTO::new).collect(Collectors.toList());

        debtsDTO.stream().forEach(debt -> {
            if (debt.getExpirationDate().isAfter(LocalDate.now())) {
                debt.setOverdue(false);
            } else {
                debt.setOverdue(true);
            }
        });

        return debtsDTO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public DebtDTO create(DebtDTO debtDTO, String condominiumUUID, UserDTO userInSession) throws Exception {

        Condominium condominium = this.condominiumService.findByUnionIdentifier(condominiumUUID);

        if (!userInSession.getUnionIdentifier().equals(condominium.getOwner().getUnionIdentifier())) {
            throw new UserIsNotCondominiumOwnerException(
                    "This operation can only be carried out by the owner of the condominium");
        }

        Debt debt = new Debt(debtDTO);
        debt.setCondominium(condominium);

        Set<UserDebt> userDebts = new HashSet<>();

        condominium.getTenants().stream().filter(tenant -> tenant.getUser() != null).forEach(tenant -> {
            UserDebt userDebt = new UserDebt();
            userDebt.setDebt(debt);
            userDebt.setTenant(tenant);
            userDebts.add(userDebt);
        });

        debt.setUserDebts(userDebts);
        this.debtDAO.save(debt);

        debtDTO.setUnionIdentifier(debt.getUnionIdentifier());
        return debtDTO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<DebtDTO> listOpenDebtsByTenant(String condominiumUUID, UserDTO userInSession) {
        List<Debt> openDebts = this.debtDAO.listOpenDebtsByTenant(userInSession.getUnionIdentifier(), condominiumUUID);

        if (openDebts == null) {
            return new ArrayList<>();
        }

        List<DebtDTO> debtsDTO = openDebts.stream().map(DebtDTO::new).collect(Collectors.toList());

        debtsDTO.stream().forEach(debt -> {
            if (debt.getExpirationDate().isAfter(LocalDate.now())) {
                debt.setOverdue(false);
            } else {
                debt.setOverdue(true);
            }
        });

        return debtsDTO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<DebtDTO> listDebtsHistory(String condominiumUUID, UserDTO userInSession) {
        List<Debt> debtsHistory = this.debtDAO.listDebtsHistory(userInSession.getUnionIdentifier(), condominiumUUID);

        if (debtsHistory == null) {
            return new ArrayList<>();
        }

        return debtsHistory.stream().map(DebtDTO::new).collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<UserDTO> listUsersByDebt(String debtUUID, UserDTO userInSession) {
        List<User> usersByDebt = this.userDebtDAO.listUsersByDebt(debtUUID, userInSession.getUnionIdentifier());

        if (usersByDebt == null) {
            return new ArrayList<>();
        }

        return usersByDebt.stream().map(UserDTO::new).collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void confirmDebtPayment(String userUUID, String debtUUID, UserDTO userInSession) throws Exception {
        UserDebt userDebtToPay = this.userDebtDAO.fetchDebtByUser(debtUUID, userUUID);
        Debt debt = this.debtDAO.findByUnionIdentifier(debtUUID);

        if (userDebtToPay == null || debt == null) {
            throw new DebitNotFoundException("Debit is not found");
        }

        if (!debt.getCondominium().getOwner().getUnionIdentifier().equals(userInSession.getUnionIdentifier())) {
            throw new UserIsNotCondominiumOwnerException(
                    "Only the owner of the condominium can carry out the operation");
        }

        userDebtToPay.setPaid(true);
        userDebtToPay.setPaidAt(LocalDateTime.now());
        this.userDebtDAO.save(userDebtToPay);
    }
}
