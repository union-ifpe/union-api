package br.com.union.service;

import br.com.union.exception.InvalidTokenException;
import br.com.union.model.User;
import br.com.union.model.UserSession;
import br.com.union.model.transport.UserDTO;
import br.com.union.model.transport.UserSessionDTO;
import br.com.union.repository.UserDAO;
import br.com.union.repository.UserSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserSessionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserSessionService.class);

    private UserSessionDAO userSessionDAO;

    private UserDAO userDAO;

    public UserSessionService(UserSessionDAO userSessionDAO, UserDAO userDAO) {
        this.userSessionDAO = userSessionDAO;
        this.userDAO = userDAO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public UserSessionDTO getSessionByToken(String token) throws InvalidTokenException {
        UserSession userSession = this.userSessionDAO.findByToken(token);
        if (userSession == null || this.isSessionExpired(userSession)) {
            throw new InvalidTokenException("Session expired, you need to login again to generate a valid token");
        }

        return new UserSessionDTO(userSession);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public UserSessionDTO createSession(UserDTO userDTO) throws Exception {
        UserSession sessionByUser = this.getSessionByUserUUID(userDTO.getUnionIdentifier());
        if (sessionByUser != null) {
            if (!this.isSessionExpired(sessionByUser)) {
                LOGGER.info("The user has an active session, returning it...");
                return new UserSessionDTO(sessionByUser);
            }
        }
        LOGGER.info("The user does not have an active session, creating...");
        User user = this.userDAO.findByUnionIdentifierAndEnabledTrue(userDTO.getUnionIdentifier());

        UserSession userSession = UserSession.newSession();
        userSession.setUser(user);
        UserSessionDTO userSessionObject = this.createSession(userSession);
        return userSessionObject;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void logoutSession(String token) {
        UserSession session = this.userSessionDAO.findByToken(token);
        if (session != null) {
            session.setLogoutDate(LocalDateTime.now());
            session.setDeleted(true);
            this.userSessionDAO.save(session);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void clearExpiredSessions() {
        List<UserSession> sessions = this.userSessionDAO.listActiveSessions();

        if (sessions != null && !sessions.isEmpty()) {
            sessions.stream().forEach(session -> this.isSessionExpired(session));
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Boolean isSessionExpired(UserSession userSession) {
        if (LocalDateTime.now().isBefore(userSession.getExpirationDate())) {
            return false;
        }
        this.userSessionDAO.delete(userSession.getToken());
        return true;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    private UserSessionDTO createSession(UserSession userSession) {
        return new UserSessionDTO(this.userSessionDAO.save(userSession));
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    private UserSession getSessionByUserUUID(String userUUID) {
        return this.userSessionDAO.findByUserUUID(userUUID);

    }

}
