package br.com.union.service;

import br.com.union.command.SendMailInvoker;
import br.com.union.core.ImagesManager;
import br.com.union.exception.*;
import br.com.union.model.*;
import br.com.union.model.enums.MailTypeEnum;
import br.com.union.model.parsers.TenantParserDTO;
import br.com.union.model.transport.CondominiumDTO;
import br.com.union.model.transport.TenantDTO;
import br.com.union.model.transport.UserDTO;
import br.com.union.repository.*;
import br.com.union.utils.ParserUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CondominiumService {

    private CondominiumDAO condominiumDAO;

    private TenantDAO tenantDAO;

    private PublicationDAO publicationDAO;

    private DebtDAO debtDAO;

    private UserDebtDAO userDebtDAO;

    private UserService userService;

    private SendMailInvoker sendMailInvoker;

    private ImagesManager imagesManager;

    public CondominiumService(CondominiumDAO condominiumDAO, TenantDAO tenantDAO, PublicationDAO publicationDAO,
                              DebtDAO debtDAO, UserDebtDAO userDebtDAO, UserService userService,
                              SendMailInvoker sendMailInvoker, ImagesManager imagesManager) {
        this.condominiumDAO = condominiumDAO;
        this.tenantDAO = tenantDAO;
        this.publicationDAO = publicationDAO;
        this.debtDAO = debtDAO;
        this.userDebtDAO = userDebtDAO;
        this.userService = userService;
        this.sendMailInvoker = sendMailInvoker;
        this.imagesManager = imagesManager;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Condominium findByUnionIdentifier(String condominiumUUID) throws Exception {
        Condominium condominium = this.condominiumDAO.findByUnionIdentifierAndDeletedFalse(condominiumUUID);

        if (condominium == null) {
            throw new CondominiumNotFoundException("Condominium with UUID: " + condominiumUUID + " is not found");
        }

        return condominium;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public CondominiumDTO findByUnionIdentifierAndUser(String condominiumUUID, UserDTO userInSession) throws Exception {
        Condominium condominium = this.condominiumDAO.findByUnionIdentifierAndDeletedFalse(condominiumUUID);

        if (condominium == null) {
            throw new CondominiumNotFoundException("Condominium with UUID: " + condominiumUUID + " is not found");
        }

        boolean isOwner = condominium.getOwner().getUnionIdentifier().equals(userInSession.getUnionIdentifier());

        if ((condominium.getTenants() == null || condominium.getTenants().isEmpty()) && !isOwner) {
            throw new UserIsNotCondominiumMemberException(
                    "The user cannot get condominium because he is not a member or owner");
        }
        Optional<Tenant> optionalTenant = condominium.getTenants().stream().filter(tenant -> tenant.getUser() != null
                && tenant.getUser().getUnionIdentifier().equals(userInSession.getUnionIdentifier())).findAny();

        if (optionalTenant.isEmpty() && !isOwner) {
            throw new UserIsNotCondominiumMemberException(
                    "The user cannot get condominium because he is not a member or owner");
        }

        CondominiumDTO condominiumDTO = new CondominiumDTO(condominium);
        condominiumDTO.setUserIsOwner(isOwner);

        Integer tenantsCount = this.condominiumDAO.getTenantsCountByCondominium(condominiumUUID);
        condominiumDTO.setTenantsCount(tenantsCount);

        Integer publicationsCount = this.publicationDAO
                .getPublicationsCountByCondominium(condominiumDTO.getUnionIdentifier());
        condominiumDTO.setPublicationsCount(publicationsCount);

        return condominiumDTO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Set<CondominiumDTO> listByUser(UserDTO userInSession, String name) throws Exception {
        List<Condominium> condominiums = this.condominiumDAO.listByUser(userInSession.getEmail(), name);

        if (condominiums == null) {
            return new HashSet<>();
        }

        Set<CondominiumDTO> condominiumDTOs = condominiums.stream().map(CondominiumDTO::new)
                .collect(Collectors.toSet());

        condominiumDTOs.stream().forEach(condominiumDTO -> {
            if (condominiumDTO.getOwner().getEmail().equals(userInSession.getEmail())) {
                condominiumDTO.setUserIsOwner(true);
            } else {
                condominiumDTO.setUserIsOwner(false);
            }

            Integer tenantsCount = this.condominiumDAO
                    .getTenantsCountByCondominium(condominiumDTO.getUnionIdentifier());

            Integer publicationsCount = this.publicationDAO
                    .getPublicationsCountByCondominium(condominiumDTO.getUnionIdentifier());
            condominiumDTO.setTenantsCount(tenantsCount);
            condominiumDTO.setPublicationsCount(publicationsCount);
        });

        return condominiumDTOs;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Set<CondominiumDTO> searchAvailablesByName(UserDTO userInSession, String name) throws Exception {
        List<Condominium> condominiums = this.condominiumDAO.listAvailablesByName(userInSession.getEmail(), name);

        if (condominiums == null) {
            return new HashSet<>();
        }

        return condominiums.stream().map(CondominiumDTO::new).collect(Collectors.toSet());
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CondominiumDTO create(CondominiumDTO condominiumDTO, UserDTO userInSession) throws Exception {

        User user = this.userService.findByUnionIdentifier(userInSession.getUnionIdentifier());

        Condominium condominium = new Condominium(condominiumDTO);
        condominium.setOwner(user);
        condominium.setCreatedAt(LocalDateTime.now());
        this.condominiumDAO.save(condominium);

        condominiumDTO.setUnionIdentifier(condominium.getUnionIdentifier());

        Map<String, Object> properties = new HashMap<>();
        properties.put("username", user.getName());
        properties.put("condominiumName", condominium.getName());
        return condominiumDTO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(String condominiumUUID, UserDTO userInSession) throws Exception {
        Condominium condominium = this.findByUnionIdentifier(condominiumUUID);

        if (!condominium.getOwner().getUnionIdentifier().equals(userInSession.getUnionIdentifier())) {
            throw new UserIsNotCondominiumOwnerException("To delete a condominium, it must be yours");
        }

        condominium.setDeleted(true);
        condominium.setDeletedDate(LocalDateTime.now());
        this.condominiumDAO.save(condominium);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void enterCondominium(String condominiumUUID, UserDTO userInSession) throws Exception {
        Condominium condominium = this.findByUnionIdentifier(condominiumUUID);

        User user = this.userService.findByUnionIdentifier(userInSession.getUnionIdentifier());

        if (condominium.getTenants() == null || condominium.getTenants().isEmpty()) {
            throw new CondominiumHasNoTenantsException("Condominium has no tenants, its not possible to enter");
        }

        Optional<Tenant> optionalTenant = condominium.getTenants().stream()
                .filter(tenant -> tenant.getEmail().equals(userInSession.getEmail())).findAny();

        if (optionalTenant.isPresent()) {
            Tenant tenant = optionalTenant.get();
            tenant.setUser(user);
            tenant.setMemberSince(LocalDateTime.now());
            this.tenantDAO.save(tenant);

            List<Debt> debtsByCondominium = this.debtDAO.findByCondominium(condominium);

            if (debtsByCondominium != null && !debtsByCondominium.isEmpty()) {
                debtsByCondominium.stream().filter(debt -> debt.getExpirationDate().isAfter(LocalDate.now()))
                        .forEach(debt -> {
                            UserDebt userDebt = new UserDebt();
                            userDebt.setDebt(debt);
                            userDebt.setTenant(tenant);

                            if (debt.getUserDebts() == null || debt.getUserDebts().isEmpty()) {
                                debt.setUserDebts(new HashSet<>(Set.of(userDebt)));
                            } else {
                                debt.getUserDebts().add(userDebt);
                            }

                            this.debtDAO.save(debt);
                        });
            }

            Map<String, Object> properties = new HashMap<>();
            properties.put("tenantEmail", userInSession.getEmail());
            properties.put("condominiumName", condominium.getName());

            this.sendMailInvoker.getCommand(MailTypeEnum.NEW_USER_ON_CONDOMINIUM).send(properties,
                    condominium.getOwner());
        } else {
            throw new UserIsNotCondominiumMemberException(
                    "The owner of the condominium must register his email in order to be able to enter");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void leaveCondominium(String condominiumUUID, UserDTO userInSession) throws Exception {
        Condominium condominium = this.findByUnionIdentifier(condominiumUUID);
		Tenant tenantByUser = this.condominiumDAO.fetchTenantByCondominiumAndUser(condominiumUUID, userInSession.getUnionIdentifier());

        if (tenantByUser == null) {
            return;
        }

        if (condominium.getOwner().getUnionIdentifier().equals(userInSession.getUnionIdentifier())) {
            throw new Exception("The owner cannot leave the condominium, just delete it");
        }

        Optional<Tenant> optionalTenant = condominium.getTenants().stream()
                .filter(tenant -> tenant.getEmail().equals(tenantByUser.getEmail())).findAny();

        if (optionalTenant.isPresent()) {
            condominium.getTenants().removeIf(tenant -> tenant.getEmail().equals(tenantByUser.getEmail()));
        }

        this.condominiumDAO.save(condominium);

        List<Debt> debts = this.debtDAO.findByCondominium(condominium);
        if (debts != null && !debts.isEmpty()) {
            debts.forEach(debt -> {
                UserDebt userDebt = this.userDebtDAO.fetchDebtByUser(debt.getUnionIdentifier(),
                        userInSession.getUnionIdentifier());
                this.userDebtDAO.delete(userDebt);
            });
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeTenant(String condominiumUUID, String userUUID, UserDTO userInSession) throws Exception {
        Condominium condominium = this.findByUnionIdentifier(condominiumUUID);
        Tenant tenantByUser = this.condominiumDAO.fetchTenantByCondominiumAndUser(condominiumUUID, userUUID);
        if (tenantByUser == null) {
            return;
        }

        if (condominium.getOwner().getUnionIdentifier().equals(userUUID)) {
            throw new Exception("The owner cannot leave the condominium, just delete it");
        }

        if (!condominium.getOwner().getUnionIdentifier().equals(userInSession.getUnionIdentifier())) {
            throw new UserIsNotCondominiumOwnerException("Only the owner can delete users");
        }

        Optional<Tenant> optionalTenant = condominium.getTenants().stream()
                .filter(tenant -> tenant.getEmail().equals(tenantByUser.getEmail())).findAny();

        if (optionalTenant.isPresent()) {
            condominium.getTenants().removeIf(tenant -> tenant.getEmail().equals(tenantByUser.getEmail()));
        }

        this.condominiumDAO.save(condominium);

        List<Debt> debts = this.debtDAO.findByCondominium(condominium);
        if (debts != null && !debts.isEmpty()) {
            debts.forEach(debt -> {
                UserDebt userDebt = this.userDebtDAO.fetchDebtByUser(debt.getUnionIdentifier(), userUUID);
                this.userDebtDAO.delete(userDebt);
            });
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<TenantDTO> insertTenants(MultipartFile file, String condominiumUUID, UserDTO userInSession)
            throws Exception {
        Condominium condominium = this.findByUnionIdentifier(condominiumUUID);

        List<TenantParserDTO> tenantsDTO = ParserUtils.getInstance().parse(file);
        Set<Tenant> newTenants = tenantsDTO.stream().map(Tenant::new).collect(Collectors.toSet());

        if (condominium.getTenants() == null || condominium.getTenants().isEmpty()) {
            condominium.setTenants(newTenants);
        } else {
            for (Tenant newTenant : newTenants) {
                Optional<Tenant> optionalTenant = condominium.getTenants().stream()
                        .filter(currentTenant -> currentTenant.getEmail().equals(newTenant.getEmail())).findAny();

                if (optionalTenant.isEmpty()) {
                    condominium.getTenants().add(newTenant);
                }
            }
        }
        return condominium.getTenants().stream().map(TenantDTO::new).collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<UserDTO> listTenants(String condominiumUUID, UserDTO userInSession) {
        Condominium condominium = this.condominiumDAO.findByUnionIdentifierAndDeletedFalse(condominiumUUID);
        if (condominium == null) {
            return new ArrayList<>();
        }

        if (!condominium.getOwner().getUnionIdentifier().equals(userInSession.getUnionIdentifier())) {
            return new ArrayList<>();
        }

        List<User> tenants = this.condominiumDAO.listTenantsByCondominium(condominiumUUID);
        if (tenants != null && !tenants.isEmpty()) {
            return tenants.stream().map(UserDTO::new).collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CondominiumDTO uploadPhoto(String unionIdentifier, MultipartFile file) throws CondominiumNotFoundException, IOException {
        Condominium condominium = condominiumDAO.findByUnionIdentifierAndDeletedFalse(unionIdentifier);

        condominium.setUrlPhotoProfile(imagesManager.sendImage(unionIdentifier, file).getBody().getUrl());

        CondominiumDTO condominiumDTO = new CondominiumDTO(condominium);

        return condominiumDTO;
    }
}
