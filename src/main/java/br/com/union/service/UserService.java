package br.com.union.service;

import br.com.union.command.SendMailInvoker;
import br.com.union.core.ImagesManager;
import br.com.union.exception.*;
import br.com.union.model.User;
import br.com.union.model.enums.MailTypeEnum;
import br.com.union.model.transport.LoginDTO;
import br.com.union.model.transport.NewPasswordDTO;
import br.com.union.model.transport.UserDTO;
import br.com.union.model.transport.UserSessionDTO;
import br.com.union.repository.UserDAO;
import br.com.union.utils.CryptographyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private UserDAO userDAO;

    private UserSessionService userSessionService;

    private IdentityConfirmationService identityConfirmationService;

    private SendMailInvoker sendMailInvoker;

    private ImagesManager imagesManager;

    public UserService(UserDAO userDAO, UserSessionService userSessionService,
                       IdentityConfirmationService identityConfirmationService, SendMailInvoker sendMailInvoker, ImagesManager imagesManager) {
        this.userDAO = userDAO;
        this.userSessionService = userSessionService;
        this.identityConfirmationService = identityConfirmationService;
        this.sendMailInvoker = sendMailInvoker;
        this.imagesManager = imagesManager;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public UserDTO getUserByToken(String token) throws Exception {
        try {
            UserSessionDTO sessionByToken = this.userSessionService.getSessionByToken(token);
            if (sessionByToken == null) {
                throw new InvalidTokenException("Token is invalid");
            }

            return sessionByToken.getUser();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new InvalidTokenException(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public User findByUnionIdentifier(String unionIdentifier) throws Exception {
        User user = this.userDAO.findByUnionIdentifierAndEnabledTrue(unionIdentifier);
        if (user == null) {
            throw new UserNotFoundException("User with UUID: " + unionIdentifier + " is not found");
        }

        return user;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public UserDTO createUser(UserDTO userDTO) throws Exception {
        LOGGER.info("Starting user registration...");
        if (this.emailExists(userDTO.getEmail())) {
            throw new EmailAlreadyRegisteredException("The email provided already belongs to an active account.");
        }

        User user = new User(userDTO);
        userDTO.setUnionIdentifier(user.getUnionIdentifier());
        userDTO.clearPassword();
        this.userDAO.save(user);

        this.sendMailInvoker.getCommand(MailTypeEnum.CONFIRMATION_ACCOUNT).send(new HashMap<>(), user);

        return userDTO;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public String login(LoginDTO loginDTO) throws Exception {
        try {
            User user = this.userDAO.findByEmail(loginDTO.getEmail());

            if (user == null) {
                throw new UserNotFoundException("User is not found. Are you already registered?");
            }

            if (!user.getEnabled()) {
                this.identityConfirmationService.deleteByUser(user);
                this.sendMailInvoker.getCommand(MailTypeEnum.CONFIRMATION_ACCOUNT).send(new HashMap<>(), user);
                throw new UserNotEnabledException(
                        "Inactive user. Check your email inbox to redeem your account activation code.");
            }

            String passToValidate = CryptographyUtils.getInstance().createPasswordWithSalt(loginDTO.getPassword(),
                    user.getSalt());
            if (!passToValidate.equals(user.getPassword())) {
                throw new InvalidPasswordException("The password provided does not match this account");
            }

            UserSessionDTO userSession = this.userSessionService.createSession(new UserDTO(user));
            return userSession.getToken();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void logout(String token) {
        this.userSessionService.logoutSession(token);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public void confirmEmailToRecoveryPassword(String email) throws Exception {
        LOGGER.info("Starting user e-mail confirmation to recovery password...");

        User user = this.userDAO.findByEmailAndEnabledTrue(email);
        if (user == null) {
            throw new UserNotFoundException("User is not found or account is not activated");
        }

        this.sendMailInvoker.getCommand(MailTypeEnum.RECOVERY_PASSWORD_CODE).send(new HashMap<>(), user);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void changePassword(NewPasswordDTO newPassDTO) throws Exception {
        User user = this.userDAO.findByEmailAndEnabledTrue(newPassDTO.getEmail());
        if (user == null) {
            throw new UserNotFoundException("User is not found or account is not activated");
        }

        this.identityConfirmationService.confirmCodeToRecoveryPassword(newPassDTO.getCode(), user);

        user.setSalt(CryptographyUtils.getInstance().generatePasswordSalt());
        user.setPassword(
                CryptographyUtils.getInstance().createPasswordWithSalt(newPassDTO.getPassword(), user.getSalt()));
        this.userDAO.save(user);

    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void alertDisabledAccounts() {
        List<User> users = this.userDAO.findByEnabledFalse();
        try {
            for (User user : users) {
                if (LocalDateTime.now().isAfter(user.getCreatedAt().plusDays(1))) {
                    this.sendMailInvoker.getCommand(MailTypeEnum.ACCOUNT_CONFIRMATION_REMINDER).send(new HashMap<>(),
                            user);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public UserDTO uploadPhoto(String unionIdentifier, MultipartFile file) throws UserNotFoundException, IOException {
        User user = userDAO.findByUnionIdentifier(unionIdentifier);

        user.setUrlPhotoProfile(imagesManager.sendImage(unionIdentifier, file).getBody().getUrl());

        UserDTO userDTO = new UserDTO(user);

        return userDTO;
    }

    private boolean emailExists(String email) {
        return this.userDAO.findByEmail(email) != null;
    }

}
