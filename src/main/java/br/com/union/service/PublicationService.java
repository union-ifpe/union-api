package br.com.union.service;

import br.com.union.exception.UserIsNotCondominiumMemberException;
import br.com.union.exception.UserIsNotCondominiumOwnerException;
import br.com.union.model.Condominium;
import br.com.union.model.Publication;
import br.com.union.model.Tenant;
import br.com.union.model.User;
import br.com.union.model.transport.PublicationDTO;
import br.com.union.model.transport.UserDTO;
import br.com.union.repository.PublicationDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PublicationService {

    private PublicationDAO publicationDAO;

    private CondominiumService condominiumService;

    private UserService userService;

    public PublicationService(PublicationDAO publicationDAO, CondominiumService condominiumService,
                              UserService userService) {
        this.publicationDAO = publicationDAO;
        this.condominiumService = condominiumService;
        this.userService = userService;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<PublicationDTO> listByUser(String condominiumUUID, UserDTO userInSession) throws Exception {
        Condominium condominium = this.condominiumService.findByUnionIdentifier(condominiumUUID);
        User user = this.userService.findByUnionIdentifier(userInSession.getUnionIdentifier());

        boolean isOwner = condominium.getOwner().getUnionIdentifier().equals(user.getUnionIdentifier());

        if ((condominium.getTenants() == null || condominium.getTenants().isEmpty()) && !isOwner) {
            throw new UserIsNotCondominiumMemberException(
                    "The user cannot list because he is not a member or owner of the condominium");
        }
        Optional<Tenant> optionalTenant = condominium.getTenants().stream().filter(tenant -> tenant.getUser() != null
                && tenant.getUser().getUnionIdentifier().equals(user.getUnionIdentifier())).findAny();

        if (optionalTenant.isEmpty() && !isOwner) {
            throw new UserIsNotCondominiumMemberException(
                    "The user cannot list because he is not a member or owner of the condominium");
        }

        return this.publicationDAO.findByCondominium(condominium).stream().map(PublicationDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public PublicationDTO create(String condominiumUUID, PublicationDTO publicationDTO, UserDTO userInSession)
            throws Exception {
        Condominium condominium = this.condominiumService.findByUnionIdentifier(condominiumUUID);
        User user = this.userService.findByUnionIdentifier(userInSession.getUnionIdentifier());

        boolean isOwner = condominium.getOwner().getUnionIdentifier().equals(user.getUnionIdentifier());

        if ((condominium.getTenants() == null || condominium.getTenants().isEmpty()) && !isOwner) {
            throw new UserIsNotCondominiumMemberException(
                    "The user cannot publish because he is not a member or owner of the condominium");
        }
        Optional<Tenant> optionalTenant = condominium.getTenants().stream().filter(tenant -> tenant.getUser() != null
                && tenant.getUser().getUnionIdentifier().equals(user.getUnionIdentifier())).findAny();

        if (optionalTenant.isEmpty() && !isOwner) {
            throw new UserIsNotCondominiumMemberException(
                    "The user cannot publish because he is not a member or owner of the condominium");
        }

        Publication publication = new Publication(publicationDTO);
        publication.setCondominium(condominium);
        publication.setUser(user);
        publication.setCreatedAt(LocalDateTime.now());

        this.publicationDAO.save(publication);

        return new PublicationDTO(publication);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(String condominiumUUID, String publicationUUID, UserDTO userInSession) throws Exception {
        Condominium condominium = this.condominiumService.findByUnionIdentifier(condominiumUUID);
        User user = this.userService.findByUnionIdentifier(userInSession.getUnionIdentifier());

        Publication publication = this.publicationDAO.findByUnionIdentifier(publicationUUID);
        if (publication == null) {
            return;
        }

        boolean isCondominiumOwner = condominium.getOwner().getUnionIdentifier().equals(user.getUnionIdentifier());
        boolean isOwner = publication.getUser().getUnionIdentifier().equals(user.getUnionIdentifier());

        if (!isCondominiumOwner && !isOwner) {
            throw new UserIsNotCondominiumOwnerException(
                    "Only the owner of the condominium or publication can delete it");
        }

        this.publicationDAO.delete(publication);
    }

}
