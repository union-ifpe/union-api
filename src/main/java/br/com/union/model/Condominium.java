package br.com.union.model;

import br.com.union.model.transport.CondominiumDTO;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
public class Condominium {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String unionIdentifier;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String address;

    @OneToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @Column(columnDefinition = "tinyint DEFAULT 0")
    private boolean deleted;

    @Column(nullable = true)
    private LocalDateTime deletedDate;

    @ManyToMany
    @Cascade(CascadeType.ALL)
    @JoinTable(name = "user_condominium", joinColumns = {@JoinColumn(name = "condominium_id")}, inverseJoinColumns = {
            @JoinColumn(name = "tenant_id")})
    private Set<Tenant> tenants;

    @Column(nullable = false)
    private LocalDateTime createdAt;

    private String urlPhotoProfile;

    public Condominium() {

    }

    public Condominium(CondominiumDTO condominiumDTO) {
        this.setUnionIdentifier(UUID.randomUUID().toString());
        this.setName(condominiumDTO.getName());
        this.setAddress(condominiumDTO.getAddress());
        this.setUrlPhotoProfile(condominiumDTO.getUrlPhotoProfile());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Set<Tenant> getTenants() {
        return tenants;
    }

    public void setTenants(Set<Tenant> tenants) {
        this.tenants = tenants;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getUrlPhotoProfile() {
        return urlPhotoProfile;
    }

    public void setUrlPhotoProfile(String urlPhotoProfile) {
        this.urlPhotoProfile = urlPhotoProfile;
    }
}
