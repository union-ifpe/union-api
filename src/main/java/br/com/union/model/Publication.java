package br.com.union.model;

import br.com.union.model.transport.PublicationDTO;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String unionIdentifier;

    @Lob
    private String message;

    @OneToOne
    @JoinColumn(name = "owner_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "condominium_id")
    private Condominium condominium;

    @Column(nullable = false)
    private LocalDateTime createdAt;

    public Publication() {

    }

    public Publication(PublicationDTO publicationDTO) {
        this.setMessage(publicationDTO.getMessage());
        this.setUnionIdentifier(UUID.randomUUID().toString());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Condominium getCondominium() {
        return condominium;
    }

    public void setCondominium(Condominium condominium) {
        this.condominium = condominium;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

}
