package br.com.union.model.parsers;

import com.opencsv.bean.CsvBindByPosition;

public class TenantParserDTO {

    @CsvBindByPosition(position = 0)
    private String email;

    @CsvBindByPosition(position = 1)
    private Integer number;

    public TenantParserDTO() {

    }

    public TenantParserDTO(String email, Integer number) {
        this.email = email;
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
