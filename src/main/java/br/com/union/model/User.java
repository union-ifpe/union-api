package br.com.union.model;

import br.com.union.model.transport.UserDTO;
import br.com.union.utils.CryptographyUtils;

import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String unionIdentifier;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String phone;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String salt;

    @Column(columnDefinition = "tinyint DEFAULT 0")
    private boolean enabled;

    @Column(nullable = false)
    private LocalDateTime createdAt;

    private String urlPhotoProfile;

    public User() {

    }

    public User(UserDTO userDTO) throws NoSuchAlgorithmException {
        this.setName(userDTO.getName());
        this.setEmail(userDTO.getEmail());
        this.setPhone(userDTO.getPhone());
        this.setUnionIdentifier(
                userDTO.getUnionIdentifier() != null ? userDTO.getUnionIdentifier() : UUID.randomUUID().toString());
        this.setEnabled(false);
        this.setSalt(CryptographyUtils.getInstance().generatePasswordSalt());
        this.setPassword(CryptographyUtils.getInstance().createPasswordWithSalt(userDTO.getPassword(), this.getSalt()));
        this.setCreatedAt(LocalDateTime.now());
        this.setUrlPhotoProfile(userDTO.getUrlPhotoProfile());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getUrlPhotoProfile() {
        return urlPhotoProfile;
    }

    public void setUrlPhotoProfile(String urlPhotoProfile) {
        this.urlPhotoProfile = urlPhotoProfile;
    }
}
