package br.com.union.model;

public class ImagesS3 {
    private String url;

    public ImagesS3() {
    }

    public ImagesS3(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
