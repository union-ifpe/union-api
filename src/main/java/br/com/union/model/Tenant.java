package br.com.union.model;

import br.com.union.model.parsers.TenantParserDTO;
import br.com.union.model.transport.TenantDTO;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Tenant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private Integer number;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(nullable = true)
    private LocalDateTime memberSince;

    @OneToMany(mappedBy = "tenant")
    private Set<UserDebt> userDebts = new HashSet<>();

    public Tenant() {

    }

    public Tenant(TenantParserDTO tenant) {
        this.email = tenant.getEmail();
        this.number = tenant.getNumber();
    }

    public Tenant(TenantDTO tenant) {
        this.email = tenant.getEmail();
        this.number = tenant.getNumber();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(LocalDateTime memberSince) {
        this.memberSince = memberSince;
    }

}
