package br.com.union.model.transport;

import br.com.union.model.Debt;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

public class DebtDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String unionIdentifier;

    @NotBlank(message = "Debt title must be provided")
    private String title;

    @NotNull(message = "Debt expiration date must be provided")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate expirationDate;

    @NotNull(message = "Debt value must be provided")
    private Double value;

    @NotBlank(message = "Debt PIX key must be provided")
    private String pixKey;

    private Boolean overdue;

    public DebtDTO() {

    }

    public DebtDTO(Debt debt) {
        this.setExpirationDate(debt.getExpirationDate());
        this.setTitle(debt.getTitle());
        this.setUnionIdentifier(debt.getUnionIdentifier());
        this.setValue(debt.getValue());
        this.setPixKey(debt.getPixKey());
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getPixKey() {
        return pixKey;
    }

    public void setPixKey(String pixKey) {
        this.pixKey = pixKey;
    }

    public Boolean getOverdue() {
        return overdue;
    }

    public void setOverdue(Boolean overdue) {
        this.overdue = overdue;
    }

}
