package br.com.union.model.transport;

import br.com.union.model.Publication;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class PublicationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String unionIdentifier;

    @NotBlank(message = "The publication message must be provided")
    private String message;

    private UserDTO user;

    private CondominiumDTO condominium;

    public PublicationDTO() {

    }

    public PublicationDTO(Publication publication) {
        this.setUnionIdentifier(publication.getUnionIdentifier());
        this.setMessage(publication.getMessage());
        this.setCondominium(new CondominiumDTO(publication.getCondominium()));
        this.setUser(new UserDTO(publication.getUser()));
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public CondominiumDTO getCondominium() {
        return condominium;
    }

    public void setCondominium(CondominiumDTO condominium) {
        this.condominium = condominium;
    }

}
