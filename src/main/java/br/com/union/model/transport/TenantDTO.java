package br.com.union.model.transport;

import br.com.union.model.Tenant;
import br.com.union.model.parsers.TenantParserDTO;

import java.io.Serializable;
import java.time.LocalDateTime;

public class TenantDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String email;

    private Integer number;

    private UserDTO user;

    private LocalDateTime memberSince;

    public TenantDTO() {

    }

    public TenantDTO(TenantParserDTO tenant) {
        this.email = tenant.getEmail();
        this.number = tenant.getNumber();
    }

    public TenantDTO(Tenant tenant) {
        this.email = tenant.getEmail();
        this.number = tenant.getNumber();

        if (tenant.getMemberSince() != null) {
            this.setMemberSince(tenant.getMemberSince());
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public LocalDateTime getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(LocalDateTime memberSince) {
        this.memberSince = memberSince;
    }
}
