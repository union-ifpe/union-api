package br.com.union.model.transport;

import br.com.union.model.Condominium;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class CondominiumDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String unionIdentifier;

    @NotBlank(message = "Condominium name is required")
    private String name;

    @NotBlank(message = "Condominium address is required")
    private String address;

    private UserDTO owner;

    private Boolean userIsOwner;

    private Integer tenantsCount;

    private Integer publicationsCount;

    private String urlPhotoProfile;


    public CondominiumDTO() {

    }

    public CondominiumDTO(Condominium condominium) {
        this.unionIdentifier = condominium.getUnionIdentifier();
        this.name = condominium.getName();
        this.address = condominium.getAddress();
        this.owner = new UserDTO(condominium.getOwner());
        this.setUrlPhotoProfile(condominium.getUrlPhotoProfile());
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserDTO getOwner() {
        return owner;
    }

    public void setOwner(UserDTO owner) {
        this.owner = owner;
    }

    public Boolean getUserIsOwner() {
        return userIsOwner;
    }

    public void setUserIsOwner(Boolean userIsOwner) {
        this.userIsOwner = userIsOwner;
    }

    public Integer getTenantsCount() {
        return tenantsCount;
    }

    public void setTenantsCount(Integer tenantsCount) {
        this.tenantsCount = tenantsCount;
    }

    public Integer getPublicationsCount() {
        return publicationsCount;
    }

    public void setPublicationsCount(Integer publicationsCount) {
        this.publicationsCount = publicationsCount;
    }

    public String getUrlPhotoProfile() {
        return urlPhotoProfile;
    }

    public void setUrlPhotoProfile(String urlPhotoProfile) {
        this.urlPhotoProfile = urlPhotoProfile;
    }
}
