package br.com.union.model.transport;

import br.com.union.model.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

public class UserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String unionIdentifier;

    @NotBlank(message = "User name must bre provided")
    private String name;

    @NotBlank(message = "User e-mail must be provided")
    @Email
    private String email;

    @NotBlank(message = "User phone must bre provided")
    private String phone;

    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$", message = "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character")
    private String password;

    private String urlPhotoProfile;

    public UserDTO() {

    }

    public UserDTO(User user) {
        this.setEmail(user.getEmail());
        this.setName(user.getName());
        this.setPhone(user.getPhone());
        this.setUnionIdentifier(user.getUnionIdentifier());
        this.setUrlPhotoProfile(user.getUrlPhotoProfile());
    }

    public void clearPassword() {
        this.setPassword(null);
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrlPhotoProfile() {
        return urlPhotoProfile;
    }

    public void setUrlPhotoProfile(String urlPhotoProfile) {
        this.urlPhotoProfile = urlPhotoProfile;
    }
}
