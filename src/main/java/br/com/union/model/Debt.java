package br.com.union.model;

import br.com.union.model.transport.DebtDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
public class Debt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String unionIdentifier;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private LocalDateTime createdAt;

    @Column(nullable = false)
    private LocalDate expirationDate;

    @Column(nullable = false)
    private Double value;

    @Column(nullable = false)
    private String pixKey;

    @OneToMany(mappedBy = "debt", cascade = CascadeType.ALL)
    private Set<UserDebt> userDebts = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "condominium_id")
    private Condominium condominium;

    public Debt() {

    }

    public Debt(DebtDTO debtDTO) {
        this.setTitle(debtDTO.getTitle());
        this.setUnionIdentifier(UUID.randomUUID().toString());
        this.setCreatedAt(LocalDateTime.now());
        this.setExpirationDate(debtDTO.getExpirationDate());
        this.setValue(debtDTO.getValue());
        this.setPixKey(debtDTO.getPixKey());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnionIdentifier() {
        return unionIdentifier;
    }

    public void setUnionIdentifier(String unionIdentifier) {
        this.unionIdentifier = unionIdentifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getPixKey() {
        return pixKey;
    }

    public void setPixKey(String pixKey) {
        this.pixKey = pixKey;
    }

    public Set<UserDebt> getUserDebts() {
        return userDebts;
    }

    public void setUserDebts(Set<UserDebt> userDebts) {
        this.userDebts = userDebts;
    }

    public Condominium getCondominium() {
        return condominium;
    }

    public void setCondominium(Condominium condominium) {
        this.condominium = condominium;
    }

}
