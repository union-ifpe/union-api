package br.com.union.command;

import br.com.union.model.User;

import java.util.Map;

public interface SendMailInterface {

    public void send(Map<String, Object> properties, User userInSession) throws Exception;
}
