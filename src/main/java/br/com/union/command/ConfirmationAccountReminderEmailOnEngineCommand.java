package br.com.union.command;

import br.com.union.core.MailManager;
import br.com.union.model.User;
import br.com.union.model.transport.MailDTO;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ConfirmationAccountReminderEmailOnEngineCommand implements SendMailInterface {

    private MailManager mailManager;

    public ConfirmationAccountReminderEmailOnEngineCommand(MailManager mailManager) {
        this.mailManager = mailManager;
    }

    @Override
    public void send(Map<String, Object> properties, User userInSession) throws Exception {
        String subject = "Lembrete: Confirmação de cadastro";
        String template = "confirmation-account-alert.html";

        properties.put("username", userInSession.getName());
        MailDTO mailDTO = new MailDTO(userInSession.getEmail(), subject, template, properties);

        this.mailManager.sendMail(mailDTO);
    }

}
