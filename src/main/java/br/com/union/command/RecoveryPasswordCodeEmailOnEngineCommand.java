package br.com.union.command;

import br.com.union.core.MailManager;
import br.com.union.model.IdentityConfirmation;
import br.com.union.model.User;
import br.com.union.model.transport.MailDTO;
import br.com.union.service.IdentityConfirmationService;
import br.com.union.utils.RandomUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class RecoveryPasswordCodeEmailOnEngineCommand implements SendMailInterface {

    private IdentityConfirmationService identityConfirmationService;

    private MailManager mailManager;

    public RecoveryPasswordCodeEmailOnEngineCommand(IdentityConfirmationService identityConfirmationService,
                                                    MailManager mailManager) {
        this.identityConfirmationService = identityConfirmationService;
        this.mailManager = mailManager;
    }

    @Override
    public void send(Map<String, Object> properties, User userInSession) throws Exception {
        String subject = "Recuperação de senha";
        String template = "password-recovery.html";

        String code = RandomUtils.getInstance().generateRandomCode();
        IdentityConfirmation accountConfirmation = new IdentityConfirmation();
        accountConfirmation.setCode(code);
        accountConfirmation.setUser(userInSession);
        this.identityConfirmationService.save(accountConfirmation);

        properties.put("username", userInSession.getName());
        properties.put("confirmationCode", code);
        MailDTO mailDTO = new MailDTO(userInSession.getEmail(), subject, template, properties);

        this.mailManager.sendMail(mailDTO);
    }

}
