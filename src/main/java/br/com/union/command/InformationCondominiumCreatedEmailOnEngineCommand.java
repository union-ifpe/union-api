package br.com.union.command;

import br.com.union.core.MailManager;
import br.com.union.model.User;
import br.com.union.model.transport.MailDTO;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class InformationCondominiumCreatedEmailOnEngineCommand implements SendMailInterface {

    private MailManager mailManager;

    public InformationCondominiumCreatedEmailOnEngineCommand(MailManager mailManager) {
        this.mailManager = mailManager;
    }

    @Override
    public void send(Map<String, Object> properties, User userInSession) throws Exception {
        String subject = "Informações sobre o condomínio criado";
        String template = "new-condominium-created.html";
        MailDTO mailDTO = new MailDTO(userInSession.getEmail(), subject, template, properties);
        mailDTO.setHasAttachment(true);

        this.mailManager.sendMail(mailDTO);
    }

}
