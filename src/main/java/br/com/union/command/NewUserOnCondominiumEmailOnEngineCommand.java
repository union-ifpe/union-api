package br.com.union.command;

import br.com.union.core.MailManager;
import br.com.union.model.User;
import br.com.union.model.transport.MailDTO;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class NewUserOnCondominiumEmailOnEngineCommand implements SendMailInterface {

    private MailManager mailManager;

    public NewUserOnCondominiumEmailOnEngineCommand(MailManager mailManager) {
        this.mailManager = mailManager;
    }

    @Override
    public void send(Map<String, Object> properties, User condominiumOwner) throws Exception {
        String subject = properties.get("condominiumName") != null
                ? "Novo participante em " + properties.get("condominiumName")
                : "Novo participante";
        String template = "user-enter-on-condominium.html";

        properties.put("username", condominiumOwner.getName());
        MailDTO mailDTO = new MailDTO(condominiumOwner.getEmail(), subject, template, properties);

        this.mailManager.sendMail(mailDTO);
    }

}
