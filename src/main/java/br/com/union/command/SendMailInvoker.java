package br.com.union.command;

import br.com.union.model.enums.MailTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SendMailInvoker {

    @Autowired
    private ApplicationContext applicationContext;

    private Map<MailTypeEnum, Class<? extends SendMailInterface>> operations = new HashMap<>();

    {
        operations.put(MailTypeEnum.CONFIRMATION_ACCOUNT, ConfirmationAccountEmailOnEngineCommand.class);
        operations.put(MailTypeEnum.CONDOMINIUM_CREATED, InformationCondominiumCreatedEmailOnEngineCommand.class);
        operations.put(MailTypeEnum.RECOVERY_PASSWORD_CODE, RecoveryPasswordCodeEmailOnEngineCommand.class);
        operations.put(MailTypeEnum.NEW_USER_ON_CONDOMINIUM, NewUserOnCondominiumEmailOnEngineCommand.class);
        operations.put(MailTypeEnum.ACCOUNT_CONFIRMATION_REMINDER,
                ConfirmationAccountReminderEmailOnEngineCommand.class);
    }

    public SendMailInterface getCommand(MailTypeEnum mailType) {
        Class<? extends SendMailInterface> clazz = operations.get(mailType);
        if (clazz == null) {
            throw new IllegalArgumentException("Command for the operation " + mailType + " is not found");
        }

        return this.applicationContext.getBean(clazz);
    }
}
